open Js_of_ocaml
open Js
open Mjs

class type ['a, 'b, 'c, 'd] vue_arg = object
  method el : js_string t readonly_prop
  method data : 'a t readonly_prop
  method computed : 'b t optdef readonly_prop
  method watch : 'c t optdef readonly_prop
  method methods : 'd t optdef readonly_prop
  method components : Unsafe.any Jstable.t optdef readonly_prop
  method router : Vue_router.router t optdef readonly_prop
end

class type ['a] test = object
end

class type ['a] vue_output = object
  inherit ['a] test
end

type ('a, 'b, 'c, 'd) vue_cs = (('a, 'b, 'c, 'd) vue_arg t -> 'a t) constr

let make
    ?computed ?(computed_js: 'b t option)
    ?watch ?(watch_js: 'c t option)
    ?methods ?(methods_js: 'd t option)
    ?data ?(data_js: 'a t option)
    ?components ?router
    id =
  let data = match data_js, data with
    | Some js, _ -> js
    | _, Some l -> object_list l
    | _ -> Unsafe.obj [||] in
  let computed = choose_js computed_js computed in
  let watch = choose_js watch_js watch in
  let methods = choose_js methods_js methods in
  let components = optdef (fun l ->
      let t = Jstable.create () in
      List.iter (fun (k, v) -> Jstable.add t (string k) v) l;
      t) components in
  let v : ('a, 'b, 'c, 'd) vue_arg t = object%js
    val el = string ("#" ^ id)
    val data = data
    val computed = computed
    val watch = watch
    val methods = methods
    val components = components
    val router = Optdef.option router
  end in
  let cs : ('a, 'b, 'c, 'd) vue_cs = Unsafe.global##._Vue in
  new%js cs v

let get_prop vm s = Unsafe.get vm @@ "$" ^ s
let get_ref vm id =
  let refs = get_prop vm "refs" in
  Unsafe.get refs id

let get_router vm id =
  get_prop vm "router"

let unhide ?(suffix="loading") id =
  let app = Dom_html.getElementById id in
  let loading = Dom_html.getElementById (id ^ "-" ^ suffix) in
  app##.style##.display := string "block";
  loading##.style##.display := string "none"

module Make(Input : sig
    type data
    val id : string
  end) = struct
  let data : any_item list ref = ref []
  let methods : any_item list ref = ref []
  let watch : any_item list ref = ref []
  let computed : any_item list ref = ref []
  let components_ref : any_item list ref = ref []

  let add_data name obj = data := !data @ [ name, Unsafe.inject obj ]
  let add_method name m = (* (fun self arg1 arg2 .. argn_n -> ) *)
    methods := !methods @ [ name, Unsafe.inject @@ wrap_meth_callback m ]
  let add_method0 name (m : Input.data t -> 'c) = add_method name m
  let add_method1 name (m : Input.data t -> 'a -> 'c) = add_method name m
  let add_method2 name (m : Input.data t -> 'a -> 'b -> 'c) = add_method name m
  let add_method3 name (m : Input.data t -> 'a -> 'b -> 'c -> 'd) = add_method name m
  let add_watch name (w : Input.data t -> 'a -> 'a -> 'c) =
    watch := !watch @ [ name, Unsafe.inject @@ wrap_meth_callback w ]
  let add_computed name (m : Input.data t -> 'a t opt) =
    computed := !computed @ [ name, Unsafe.inject @@ wrap_meth_callback (fun app () -> m app)]
  let add_component name c = components_ref := !components_ref @ [ name, Unsafe.inject c ]

  let app : Input.data t ref = ref (Unsafe.obj [||])

  let init ?(export=true) ?(data_js : Input.data t option) ?(show=false) ?suffix ?(components=[]) ?router () =
    let data = match data_js with None -> Some !data | Some _ -> None in
    let components = match !components_ref @ components with [] -> None | l -> Some l in
    app :=
      make
        ?data ?data_js (* data_js is safer for typecheck but constraining *)
        ~methods:!methods
        ~watch:!watch
        ~computed:!computed
        ?components
        ?router
        Input.id;
    if show then unhide ?suffix Input.id;
    if export then Js.export Input.id !app;
    !app
end

(** Vue Components *)

let component = Vue_component.make

module Component = Vue_component.Make
