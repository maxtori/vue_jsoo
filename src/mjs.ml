open Js_of_ocaml
open Js

let object_list data = Unsafe.obj @@ Array.of_list data

let optdef f = function
  | None -> undefined
  | Some x -> def (f x)

let choose_js js l = match js, l with
  | Some js, _ -> def js
  | _, l -> optdef object_list l

let array l = array @@ Array.of_list l

let listf f l = array @@ List.map f l

type any_item = string * Unsafe.any
