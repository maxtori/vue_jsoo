open Js_of_ocaml
open Js
open Mjs

type component = Unsafe.any
let to_component = Unsafe.inject

class type name = object
  method name : js_string t readonly_prop
end

class type route = object
  method path : js_string t readonly_prop
  method component : component optdef readonly_prop
  method children : route t js_array t optdef readonly_prop
  method name : js_string t optdef readonly_prop
  method components : component Jstable.t optdef readonly_prop
  method redirect : Unsafe.any optdef readonly_prop
  method alias : js_string t optdef readonly_prop
  method props : bool t optdef readonly_prop
end

type redirect = Rd of string | Rd_name of string
let to_redirect = function
  | Rd s -> Unsafe.inject (string s)
  | Rd_name s -> Unsafe.inject (object%js val name = string s end : name t)

type route_ml = {
  path : string;
  component : Unsafe.any Vue_component.component option;
  children : route_ml list option;
  name : string option;
  components : (string * Unsafe.any Vue_component.component) list option;
  redirect : redirect option;
  alias : string option;
  props : bool option;
}

let empty = {
  path = ""; component = None; children = None; name = None; components = None;
  redirect = None; alias = None; props = None }

let rec make_route_base {path; component; children; name; components; redirect; alias; props} : route t =
  let components = optdef (fun l ->
      let t = Jstable.create () in
      List.iter (fun (k, v) ->
          Jstable.add t (string k) (Unsafe.inject @@ Vue_component.make_arg_base v)) l;
      t) components in
  let children = optdef (fun l -> listf make_route_base l) children in
  object%js
    val path = string path
    val component = optdef (fun c -> Unsafe.inject @@ Vue_component.make_arg_base c) component
    val children = children
    val name = optdef string name
    val components = components
    val redirect = optdef to_redirect redirect
    val alias = optdef string alias
    val props = optdef bool props
  end

let make_route ?component ?children ?name ?components ?redirect ?alias ?props path =
  make_route_base {path; component; children; name; components; redirect; alias; props}

class type router_args = object
  method routes : route t js_array t readonly_prop
  method mode : js_string t optdef readonly_prop
end

class type push_args = object
  method path : js_string t optdef readonly_prop
  method name : js_string t optdef readonly_prop
  method params : js_string t Jstable.t optdef readonly_prop
  method query : js_string t Jstable.t optdef readonly_prop
end

class type router = object
  method push : js_string t -> unit meth
  method push_obj : push_args t -> unit meth
  method replace : js_string t -> unit meth
  method replace_obj : push_args t -> unit meth
  method go : int -> unit meth
end

let make_args_base ?mode routes : router_args t =
  object%js
    val routes = array routes
    val mode = optdef string mode
  end

let make_args ?mode routes =
  let routes = List.map make_route_base routes in
  make_args_base ?mode routes

let vue_router_cs : (router_args t -> router t) constr = Unsafe.variable "VueRouter"

let make_base ?mode routes =
  new%js vue_router_cs (make_args_base ?mode routes)

let make ?mode routes =
  new%js vue_router_cs (make_args ?mode routes)
