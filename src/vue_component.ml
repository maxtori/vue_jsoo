open Js_of_ocaml
open Js
open Mjs

(** ml types *)

type js_type =
  | JString of string
  | JObject of Unsafe.any
  | JNumber of float
  | JBoolean of bool

type prop_object = {
  pr_default : js_type;
  pr_required : bool option;
  pr_validator : (string -> bool) option;
}

type prop_options =
  | PrType of js_type
  | PrTypeArray of js_type list
  | PrObj of prop_object

type props_options = PrsArray of string list | PrsObj of (string * prop_options) list

type 'a component = {
  template : string option;
  props : props_options option;
  data : 'a t option;
  render : (Unsafe.any -> Unsafe.any -> Unsafe.any) option;
  computed : any_item list option;
  watch : any_item list option;
  methods : any_item list option;
  mixins : Unsafe.any list option;
  extends : Unsafe.any option;
  mounted : (Unsafe.any -> unit) option;
}

let empty = {
  template = None; props = None; data = None; render = None; computed = None;
  watch = None; methods = None; mixins = None; extends = None; mounted = None;
}

(** js types *)

class type prop_object_js = object
  method type_ : (js_string t -> Unsafe.any t) callback readonly_prop
  method default : Unsafe.any optdef readonly_prop
  method required : bool t optdef readonly_prop
  method validator : (js_string t -> bool t) callback optdef readonly_prop
end

class type ['a] component_arg = object
  method template : js_string t optdef readonly_prop
  method props : Unsafe.any optdef readonly_prop
  method data : (Unsafe.any, unit -> 'a t) meth_callback optdef readonly_prop
  method render : (Unsafe.any, Unsafe.any -> Unsafe.any) meth_callback optdef readonly_prop
  method computed : Unsafe.any optdef readonly_prop
  method watch : Unsafe.any optdef readonly_prop
  method methods : Unsafe.any optdef readonly_prop
  method mixins : Unsafe.any js_array t optdef readonly_prop
  method extends : Unsafe.any optdef readonly_prop
  method mounted : (Unsafe.any, unit) meth_callback optdef readonly_prop
end

type 'a vue_output = 'a t

class type ['a] vue_object = object
  method component : js_string t -> 'a component_arg t optdef -> 'a vue_output meth
  method component_extend : js_string t -> 'a component_arg t optdef -> 'a vue_output constr meth
end

(** ml to js *)

let js_type_cs : (js_type -> (js_string t -> Unsafe.any t) callback) = function
  | JString _ -> wrap_callback @@ Unsafe.variable "String"
  | JObject _ -> wrap_callback @@ Unsafe.variable "Object"
  | JNumber _ -> wrap_callback @@ Unsafe.variable "Number"
  | JBoolean _ -> wrap_callback @@ Unsafe.variable "Boolean"

let js_prop_obj {pr_default; pr_required; pr_validator} : prop_object_js t =
  let default = match pr_default with
    | JString s -> def @@ Unsafe.inject (string s)
    | JNumber f -> def @@ Unsafe.inject (number_of_float f)
    | JBoolean b -> def @@ Unsafe.inject (bool b)
    | JObject o -> def @@ Unsafe.inject o in
  object%js
    val type_ = js_type_cs pr_default
    val default = default
    val required = optdef bool pr_required
    val validator = optdef (fun v -> wrap_callback (fun s -> bool (v (to_string s)))) pr_validator
  end

let make_prop = function
  | PrType s -> Unsafe.inject @@ js_type_cs s
  | PrTypeArray a -> Unsafe.inject @@ listf js_type_cs a
  | PrObj o -> Unsafe.inject @@ js_prop_obj o

let make_props = function
  | PrsObj l ->
    let t = Jstable.create () in
    List.iter (fun (name, pr) -> Jstable.add t (string name) (make_prop pr)) l;
    Unsafe.inject t
  | PrsArray l ->
    Unsafe.inject (listf string l)

(** make component arguments *)

let make_arg_base c : 'a component_arg t =
  object%js
    val template = optdef string c.template
    val props = optdef make_props c.props
    val data = optdef (fun d -> wrap_meth_callback (fun _ () -> d)) c.data
    val render = optdef wrap_meth_callback c.render
    val computed = optdef object_list c.computed
    val watch = optdef object_list c.watch
    val methods = optdef object_list c.methods
    val mixins = optdef array c.mixins
    val extends = Optdef.option c.extends
    val mounted = optdef wrap_meth_callback c.mounted
  end

let make_arg ?template ?render ?props ?data ?computed ?methods ?watch ?mixins
    ?extends ?mounted () =
  make_arg_base {template; props; data; render; computed; watch; methods; mixins;
                 extends; mounted}

(** make component *)

let make ?template ?render ?props ?data ?computed ?methods ?watch ?mixins
    ?extends ?mounted name =
  let arg = make_arg ?template ?render ?props ?data ?computed ?methods ?watch
      ?mixins ?extends ?mounted () in
  let v : 'a vue_object t = Unsafe.global##._Vue in
  v##component (string name) (def arg)

let extend ?template ?render ?props ?data ?computed ?methods ?watch ?mixins
    ?extends ?mounted name =
  let arg = make_arg ?template ?render ?props ?data ?computed ?methods ?watch
      ?mixins ?extends ?mounted () in
  let v : 'a vue_object t = Unsafe.global##._Vue in
  let cs = v##component_extend (string name) (def arg) in
  new%js cs

(** functor *)

module Make(S : sig
    val name : string
    val template : string option
    val props : props_options option
    type data
  end) = struct
  let methods : any_item list ref = ref []
  let watch : any_item list ref = ref []
  let computed : any_item list ref = ref []

  let add_method name m = (* (fun self arg1 arg2 .. argn_n -> ) *)
    methods := !methods @ [ name, Unsafe.inject @@ wrap_meth_callback m ]
  let add_method0 name (m : S.data t -> 'c) = add_method name m
  let add_method1 name (m : S.data t -> 'a -> 'c) = add_method name m
  let add_method2 name (m : S.data t -> 'a -> 'b -> 'c) = add_method name m
  let add_method3 name (m : S.data t -> 'a -> 'b -> 'c -> 'd) = add_method name m
  let add_watch name (w : S.data t -> 'a -> 'a -> 'c) =
    watch := !watch @ [ name, Unsafe.inject @@ wrap_meth_callback w ]
  let add_computed name (m : S.data t -> 'a t opt) =
    computed := !computed @ [ name, Unsafe.inject @@ wrap_meth_callback (fun app () -> m app)]

  let component : S.data t ref = ref (Unsafe.obj [||])

  let load ?(export=true) ?(data : S.data t option) () =
    component := make ?data ?template:S.template ?props:S.props
        ~methods:!methods ~watch:!watch ~computed:!computed S.name;
    if export then Js.export S.name !component;
    !component

  let get () = !component
end
