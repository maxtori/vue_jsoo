all:
	dune build

test: all
	cp -f _build/default/test/test_vue.bc.js test/test-vue.js
	cp -f _build/default/test/test_router.bc.js test/test-router.js
	cp -f _build/default/test/test_chart.bc.js test/test-chart.js

clean:
	dune clean

install:
	dune install

doc:
	dune build @doc
	cp -rf _build/default/_doc/_html/* docs
